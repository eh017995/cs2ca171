package Routing;
/**
 * This program demonstrates DV routing algorithm based on Bellman-Ford's single source shortest path
 * @author jl922223
 * @version 1.0
 * @since 2020-12-12
 */
import java.util.*; 
import java.lang.*; 
import java.io.*; 

//A class to represent a connected, directed and costed graph 
public class DVRouting { 
	// A class to represent a costed edge in graph 
	class Edge { 
		int src, dest, cost; 
		Edge() 
		{ 
			src = dest = cost = 0; 
		} 
	}; 

	int V, E; 
	Edge edge[]; 

	// Creates a graph with V vertices and E edges 
	DVRouting(int v, int e) 
	{ 
		V = v; 
		E = e; 
		edge = new Edge[e]; 
		for (int i = 0; i < e; ++i) 
			edge[i] = new Edge(); 
	} 

	// The main function that finds shortest distances from src 
	// to all other vertices using Bellman-Ford algorithm. The 
	// function also detects negative cost cycle 
	void BellmanFord(int src, DVRouting graph) 
	{ 
		int V = graph.V, E = graph.E; 
		int dist[] = new int[V]; 

		// Step 1: Initialize distances from src to all other 
		// vertices as INFINITE 
		for (int i = 0; i < V; ++i) 
			dist[i] = Integer.MAX_VALUE; 
		dist[src] = 0; 

		// Step 2: Relax all edges |V| - 1 times. A simple 
		// shortest path from src to any other vertex can 
		// have at-most |V| - 1 edges 
		for (int i = 1; i < V; ++i) { 
			for (int j = 0; j < E; ++j) { 
				int u = graph.edge[j].src; 
				int v = graph.edge[j].dest; 
				int cost = graph.edge[j].cost; 
				if (dist[u] != Integer.MAX_VALUE && dist[u] + cost < dist[v]) 
					dist[v] = dist[u] + cost; 
			} 
		} 

		// Step 3: check for negative-cost cycles. The above 
		// step guarantees shortest distances if graph doesn't 
		// contain negative cost cycle. If we get a shorter 
		// path, then there is a cycle. 
		for (int j = 0; j < E; ++j) { 
			int u = graph.edge[j].src; 
			int v = graph.edge[j].dest; 
			int cost = graph.edge[j].cost; 
			if (dist[u] != Integer.MAX_VALUE && dist[u] + cost < dist[v]) { 
				System.out.println("Graph contains negative cost cycle"); 
				return; 
			} 
		} 
		printArr(src, dist, V); 
	} 

	// A utility function used to print the solution 
	void printArr(int src, int dist[], int V) 
	{ 
		for (int i = 0; i < V; ++i) 
            System.out.println(String.format("Distance from source vertex %s to vertex %s is %s", src, i, dist[i]));
	}  

	// Driver method to test above function 
	public static void main(String[] args) 
	{ 
		int V = 5; // Number of vertices in graph 
		int E = 7; // Number of edges in graph 

		DVRouting graph = new DVRouting(V, E); 

		// add edge 0-1
		graph.edge[0].src = 0; 
		graph.edge[0].dest = 1; 
		graph.edge[0].cost = 4; 

		//finsh the code by adding the edges as in edge 0-1
		// add your code here
		// add edge 0-2 

		// add edge 1-2 


		// add edge 1-3 


		// add edge 2-3 


		// add edge 3-4 


		// add edge 2-4 


		graph.BellmanFord(graph, 0); 
	} 
}