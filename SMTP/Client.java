
package SMTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * 
 * Represents an SMTP Client application. 
 * Stores all information required to define the client/host and connect to a Server.
 * Comprised of 2 methods for sending and listening to TCP messages through sockets.
 * 
 * @author JamesWalford
 *
 */
public class Client {

	private Socket tcpSocket;	            // A Socket (connects to the ServerSocket)
    private InetAddress serverAddress;	    // IP address of the server
    private int serverPort;	                // The port number of the ServerSocket
    private Scanner scanner;	            // Scans for user input
    private static String senderEmail;	    // Senders email address
    private static String recieverEmail;	// Receivers email address
    private int stageOfTransmission; 		// Indicates current stage of SMTP protocol  
    private static boolean connectionOpen;  // Indicates connection status of Sockets
    

/**
 * Client Constructor: Initialises an object representing and storing all data defining and required by an SMTP client application.
 * Stores
 * 	-> The server address
 *  -> The server port
 *  -> An instantiated Socket (Created using Server IP and Port #)
 *  -> A Scanner (Scans for user input)
 *  -> Default SMTP stage 
 * 
 * @param serverAddress Server IP Address
 * @param serverPort ServerSocket port #
*/
	public Client (InetAddress serverAddress, int serverPort) throws Exception {	
        this.serverAddress = serverAddress; 	 								// Stores server address
        this.serverPort = serverPort;											// Stores server port
        this.tcpSocket = new Socket(this.serverAddress, this.serverPort);		// Creates a socket (TCP connection to specified ServerSocket)
        this.scanner = new Scanner(System.in);									// Scans System.in for user input
        this.stageOfTransmission = 0;											// Sets default SMTP stage indicator
	}
	
	/**
	 * This method accepts a string argument and sends it as a message to the server.
	 * The method starts by getting the output stream of the socket.
	 * Then it encodes the string message as bytes using the US-ASCII character set.
	 * This newly encoded message is then written to the output stream and sent to the server.
	 * 
	 * @param msg String message to send
	*/
    private void send(String msg) throws Exception {
    	OutputStream out = this.tcpSocket.getOutputStream();
    	out.write((msg+"\r\n").getBytes("US-ASCII"));
    	
    }
   
    /**
     * This method listens for TCP messages sent by the server and determines an appropriate response.
     * Messages are received using Client objects socket, they are then decoded and turned into string tokens.
     * The created String tokens are then used to determine an appropriate response message, requesting user input where required. 	
     */
    private void listen() throws Exception {
    	InputStream is = this.tcpSocket.getInputStream();	// Gets the input stream of the socket
    	BufferedReader br = new BufferedReader(new InputStreamReader(is, "US-ASCII")); // Reads and decodes incoming messages
    	String requestLine = br.readLine(); // Reads and stores next line of the input stream (Next message received)
    	System.out.println("\r\nMessage from " + this.tcpSocket.getInetAddress().getHostAddress() + ": " + requestLine); // Outputs message to user
    	String command;
    	
    	StringTokenizer tokens = new StringTokenizer(requestLine); // Splits string into tokens
    	String tokenValue = tokens.nextToken(); // Gets the first token value
    	
    	
    	// tokenValue is used to check what Telnet response was received
    	if (tokenValue.contentEquals("220")) {	// 220 - SMTP Service Ready
    		command = "HELLO" + " "  + senderEmail.split("@")[1].replace(">", ""); // Send back HELLO + domain name
    		send(command);
    	}
    	
    	else if (tokenValue.contentEquals("250")) { // 250 - Requested action taken and completed
    		
    		stageOfTransmission = stageOfTransmission + 1; // Increment the stage of SMTP mail transfer
    		
    		switch (stageOfTransmission) {
    		
    		case 1 : // Stage 1
    			command = "MAIL FROM: " + senderEmail; // Construct message specifying sender email address
    			send(command); // Send message
    			break;
    		case 2:	// Stage 2
    			command = "RCPT TO: " + recieverEmail; // Construct message specifying recipient email address 
    			send(command); // Send message
    			break;
    		case 3: // Stage 3
    			command = "DATA"; // Construct message specifying that the email content will be sent
    			send(command); // Send message
    			break;
    		case 4:	// Stage 4
    			command = "QUIT"; // Construct message specifying that the client is finished
    			send(command); // Send message
    		}
    		
    	} else if (tokenValue.contentEquals("End")) { // Server is ready to accept mail content	
    			
    			String data = ""; 
    			System.out.println("\n	Enter Message: "); // Request user input
 
    			while (!data.contentEquals(".")) { // While user has not input "."
    				
    				data = scanner.nextLine(); // Scan and store user input in data variable
  
    				send(data); // Send the data
    			}
    						
    	} else if (tokenValue.contentEquals("221")) { // 221 - Service Closing
			tcpSocket.close(); // Close socket
			connectionOpen = false;	// Indicate that the connection is no longer operational
    	} 
    	else {	
    		System.out.println("Unrecognised Responce"); // Catch unrecognised Telnet messages
    	} 
    } 

    /**
     * Client Application main method.
     * The main method accepts a server ip address and port number as arguments
     * If arguments are provided it overrides the base values.
     * These values are used to instantiate a Client object
     * Also required user input is requested, formatted and stored.
     * 
     * @param args The command line arguments.
     * @throws Exception
     **/
	public static void main(String args[]) throws Exception {
		
		InetAddress serverIP = InetAddress.getByName("192.168.56.1"); // Base server address
		int port = 25; // Base ServerSocket port number
		
		if (args.length > 0) { // If arguments have been provided: override base values
			serverIP = InetAddress.getByName(args[0]); 
			port = Integer.parseInt(args[1]);
		}
		
		Client client = new Client(serverIP, port); // Instantiate Client object, triggers constructor
		
        System.out.println("\r\nConnected to Server: " + client.tcpSocket.getInetAddress()); // Output that the connection was successful
        
        connectionOpen = true; // Indicate that the connection is open
        
        Scanner initialScanner = new Scanner(System.in); // scanner to read user input
		System.out.println("\nEnter Sender Email: "); // Request and store sender email
		senderEmail = "<" + initialScanner.nextLine() + ">";
		System.out.println("\nEnter Reciver Email: "); // Request and store recipient email
		recieverEmail = "<" + initialScanner.nextLine() + ">";
		
		// While the connection is operational
		while (connectionOpen) {	
        	client.listen(); // Run the listen() method
        }
		
		initialScanner.close(); // When the connection is not operational, close the scanner (and underlying System.in)
		
	}
	
}