package TCPSocket;
import java.io.IOException;
/**
 * This program demonstrates a TCP client
 * @author jl922223
 * @version 1.0
 * @since 2020-12-12
 */
public class TCPClient{
    private Socket tcpSocket;
    private InetAddress serverAddress;
    private int serverPort;
    private Scanner scanner;
    /**
     * @param serverAddress
     * @param serverPort
     * @throws Exception
     */
    private TCPClient(InetAddress serverAddress, int serverPort) throws Exception {       
        //Initiate the connection with the server using Socket. 
        //For this, creates a stream socket and connects it to the specified port number at the specified IP address. 
        //add your code here
    }
    
    /**
     * The start method connect to the server and datagrams 
     * @throws IOException
     */
    private void start() throws IOException {
        //create a new PrintWriter from an existing OutputStream (i.e., tcpSocket). 
        //This convenience constructor creates the necessary intermediateOutputStreamWriter, which will convert characters into bytes using the default character encoding
        //You may add your code in a loop so that client can keep send datagrams to server
        //add your code here
    }
    
    public static void main(String[] args) throws Exception {
		// set the server address (IP) and port number
    	InetAddress serverIP = InetAddress.getByName("192.168.56.1"); // local IP address
    	int port = 7077;
		
		if (args.length > 0) {
			serverIP = InetAddress.getByName(args[0]);
			port = Integer.parseInt(args[1]);}
		// call the constructor and pass the IP and port
		//add your code here		
    }
}