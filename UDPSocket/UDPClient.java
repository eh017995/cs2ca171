package UDPSocket;
/**
 * This program demonstrates a UDP client
 * @author jl922223
 * @version 1.0
 * @since 2020-12-12
 */
public class UDPClient {
    private InetAddress ipAddress;
    private int sendPort;
    private int recvPort;

    private UDPClient(InetAddress serverAddress, int sendPort, int recvPort) throws Exception {
        this.ipAddress = serverAddress;
        this.sendPort = sendPort;
        this.recvPort = recvPort;
    }
    
    /**
     * The start method initiates the communication by sending datagram to the server
     * @throws IOException
     */
    private void start() throws IOException {
    	// Initiate the Datagram Socket 
    	DatagramSocket udpSocket = new DatagramSocket();
    	
    	//Take user input using, e.g., useing scanner 
		// add your code here
        
		/* Encapsulate user input in a Datagram Packet using DatagramPacket
		 * for DatagramPacket you need Parameters:
		 * send packet. 
		 * data.length = packet length.
		 * address = destination address (IP and port).
		 */
		// add your code here
        
        // Send the packet using DatagramSocket Initiated object
        // add your code here   
		
        // Close the DatagramSocket
        udpSocket.close();
        
        // Call listenToServer to listen to incoming messages from the server
        listenToServer();
    }
    
	/**
	 * The listenToServer method listen to the client port for incoming messages from the server
	 * @throws IOException
	 */
	private void listenToServer() throws IOException {
		// Initiating the Datagram Socket on receive port
        // add your code here
		
		byte[] buf = new byte[256];
		/* Initiating DatagramPacket - Parameters are:
		 * 1-buffer for holding the incoming datagram.
		 * 2-length the number of bytes to read.
		 */
		 // add your code here
        
        // Fetch the packet data using DatagramSocket object receive method
        // add your code here  
        
        // Print the message contents
        String msg = new String(packet.getData()).trim();
        System.out.println("Message from Server: " + msg);
        
        // Close the DatagramSocket
        udpSocket.close();
	}

	public static void main(String[] args) throws Exception {
		// set the client address (IP) and send/receive ports
		System.out.println("-- Running UDP Client at " + InetAddress.getLocalHost() + " --");
		InetAddress ipAddress = InetAddress.getLocalHost();  // local IP address
		int sendPort = 7077; // use to send datagram out to server
		int recvPort = 7076; // use to listen to datagram sent from server
		UDPClient client = new UDPClient(ipAddress, sendPort, recvPort);
		client.start();
	}
}