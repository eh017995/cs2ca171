package WWWServer;
/**
 * This program demonstrates a WWW server
 * Complete when it say "add your code here"
 * @author jl922223
 */
 public final class WebServer {
	 
	public static void main(String argv[]) throws Exception {
		// Get the port number from the command line.
		// add your code here

		// Establish the listen socket using the port number.
		// add your code here
		
		// Process HTTP service requests in an infinite loop => while(true)
		while(true) {
			// Listen for a TCP connection request.
			// add your code here


			// Construct an object to process the HTTP request message.
			// add your code here


			// Create a new thread to process the request and starting the thread. No need to update
			Thread thread = new Thread(request);
			thread.start();		
		}
	}
}